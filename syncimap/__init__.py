# -*- coding: utf-8 -*-
#
# This file is part of syncimap.
# 
#  syncimap is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  syncimap is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
from syncimap.syncimap import Syncimap
__all__ = ["Syncimap"]

__LICENSE__ = 'GPLv3+'
__VERSION__ = '0.0.0'
__DESCRIPTION__ = """Syncimap  version {}. Synchronize an IMAP server with another IMAP server or a local Maildir. Licensed under the GNU GPLv3 (or any later version).""".format(__VERSION__)
