# -*- coding: utf-8 -*-
#
# This file is part of syncimap.
# 
#  syncimap is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  syncimap is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
import configparser
import functools
import os
import logging
import argparse
import signal
from weakref import ref

from .account import Account

class Syncimap:
    """Main class which just needs to be invoked

    :important instance variables:
    config: ConfigParser instance of our configuration
    args: command line arguments
    account_tasks: list of Tasks with `Accounts` to be synced"""

    def __init__(self, cmdopts=None):
       """Invoke with a set of commands

       :param cmdopts: string with cmdline options, if None, the
                                    cmdline options will be determined.

       """
       # Parse the command line options and save them
       # Then, find the right configuration file to use
       # read it in and modify its values with cmd line options
       if cmdopts is None:
           self.args = self.parse_cmdline()

       conffile = self.find_config_file(self.args.conffile)
       self.config = self.parse_config_file(conffile, self.args)
       # List of accounts that needs syncing
       self.account_tasks = []
       self.run_main_loop()


    def find_config_file(self, conffile=None):
        """Return location of the config file to be used.
        Use explicitely set config file, or find the right one"""
        if conffile is None:
            xdg_conf = os.getenv("XDG_CONFIG_HOME", default="~/.config")
            basedirs = ['.', os.path.join(xdg_conf, "syncimap"), '/etc']
            for basedir in basedirs:
                confpath = os.path.join(basedir, "syncimap.conf")
                confpath = os.path.abspath(os.path.expanduser(confpath))
                if os.path.exists(confpath):
                    conffile = confpath
                    break
                logging.debug("Did not find a config file at '{}'".format(confpath))
            
        if conffile is None or not os.path.exists(conffile):
            logging.error("No configuration file found.")
            raise ValueError("No configuration file found. Please see http://XXX")
        logging.debug("Using config file {}".format(conffile))
        return conffile

    def parse_config_file(self, conffile, args):
        """Parse the file and override its values with command line values"""
        config = configparser.ConfigParser()
        config.read(conffile, encoding='utf8')
        return config


    def parse_cmdline(self):
        """Parse the command line, also read and parse the config file as part of that"""
        from syncimap import __VERSION__, __LICENSE__, __DESCRIPTION__
        parser = argparse.ArgumentParser(description=__DESCRIPTION__)
        #parser.add_argument('integers', metavar='N', type=int, nargs='+',
        #                   help='an integer for the accumulator')
        #parser.add_argument('--sum', dest='accumulate', action='store_const',
        #                   const=sum, default=max,
        #                    help='sum the integers (default: find the max)')
        parser.add_argument('--config', nargs='?', dest='conffile', default=None,
                            help='Use this configuration file')
        args = parser.parse_args()
        return args
        #print(args.accumulate(args.integers))

    def signal_handler(self, signame):
        """Handler for SIGINT and SIGTERM
        This should probably schedule a coroutine that cleans up stuff and exits then"""
        logging.error("got signal %s: exit" % signame)
        self.loop.stop()

    def setup_accounts(self):
        """Schedule the accounts that we want to sync
        Coroutine which suspends if the self.accounts queue is full, ie the current max number of concurrent syncs happens."""
        for sec in self.config.sections():
            if not sec.lower().startswith("account "): continue
            if self.config.getboolean(sec, "disabled", fallback=False): continue
            name = sec[:8] # Account name from config section name
            acc = Account(name, self, self.config)
            self.account_tasks.append(asyncio.async(acc.sync))
            print (acc.sync)

    def wakeup_python34_onwin(self):
        """ 
        Hacky workaround from https://bugs.python.org/issue23057
        The bug has apparently be fixed in Python 3.5, so should be scheduled on Python 3.4 and on Windows only"""
        self.loop.call_later(0.5, self.wakeup_python34_onwin)

    def run_main_loop(self):
        """Enter the main loop"""
        self.loop = asyncio.get_event_loop()

        # Add signal handler, (except on Windows where Unix signals don't work)
        if os.name is 'nt':
            # Wakeup every 0.5 sec to allow crappy Windows to process SIGINT (ctrl-c). See comments in that function.
            self.loop.call_later(0.5, self.wakeup_python34_onwin)
        else:
            for signame in ('SIGINT', 'SIGTERM'):
                self.loop.add_signal_handler(getattr(signal, signame),
                                             functools.partial(self.signal_handler, signame))

        self.setup_accounts()

        # Blocking call interrupted by loop.stop()
        logging.error("Running main loop now")
        self.loop.run_until_complete(asyncio.wait(self.account_tasks))
        self.loop.close()
